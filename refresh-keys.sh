#!/bin/bash
set -e
set -x

rm -f ./trusted-repo-keys.gpg || true
rm -f /tmp/trusted-repo-keys.gpg || true

for file in *.gpg
do
	echo "Processing $file"
	rm ../update.gpg || true
	gpg --no-default-keyring --keyring ../update.gpg --fingerprint --import "${file}"
	gpg --no-default-keyring --keyring ../update.gpg --keyserver keyserver.ubuntu.com --refresh-keys || true
	gpg --no-default-keyring --keyring ../update.gpg --keyserver keys.openpgp.org --refresh-keys || true
	#gpg --no-default-keyring --keyring ../update.gpg --keyserver pgp.mit.edu --refresh-keys || true
	gpg --no-default-keyring --keyring ../update.gpg --export > "${file}"
	git add "${file}" || true
	git commit -m "update ${file}" || true
	
	gpg --no-default-keyring --keyring /tmp/trusted-repo-keys.gpg --import "${file}" || true
done

gpg --no-default-keyring --keyring /tmp/trusted-repo-keys.gpg --export > ./trusted-repo-keys.gpg || true
rm -f /tmp/trusted-repo-keys.gpg || true
git add trusted-repo-keys.gpg || true
git commit -m "update trusted-repo-keys.gpg" || true
