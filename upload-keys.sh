#!/bin/bash
set -e
set +x

file=trusted-repo-keys.gpg

echo "Processing $file"
rm ../upload.gpg || true
gpg --no-default-keyring --keyring ../upload.gpg --fingerprint --import "${file}"

pubkeys=`gpg --no-default-keyring --keyring ../upload.gpg --list-public-keys --with-colons | sed -ne '/^pub:/,/^fpr:/ { /^fpr:/ p }' | cut -d: -f10`

while IFS= read -r line; do
	gpg --no-default-keyring --keyring ../upload.gpg --keyserver keyserver.ubuntu.com --send-keys $line || true
	gpg --no-default-keyring --keyring ../upload.gpg --keyserver keys.openpgp.org --send-keys $line || true
	#gpg --no-default-keyring --keyring ../upload.gpg --keyserver pgp.mit.edu --send-keys $line || true
done <<< "$pubkeys"
